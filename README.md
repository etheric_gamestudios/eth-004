# Catz Jump
TODO : trouver un nom meilleur

## Roadmap 1

Système de grille 4x4 cases
Taille max des sprites pour 1 case : 200x200px

- Sprites chat
- Menu (background + UI)
- Font (à faire ou [à trouver](https://www.dafont.com/fr/))
- Boutique d'objets
    * Fusée de démarrage
    * Skins
    * ?
- Pièges
    * Scie horizontale / verticale
    * Lance-flamme sur 1 côté
    * Boule à chaîne (morgenstern)
    * Cigogne (piège vertical)
- Bonus
    * Coin +5
    * Bulle d'invincibilité pour 1 touche + avec timer
    * Canon
    * Gros bonus avec invincibilité + projection verticale