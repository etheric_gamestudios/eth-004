import numpy


def compute(init_pos: numpy.array, target_pos: numpy.array, time_of_flight: float, gravity: float = 1960) -> numpy.array:
    """
    Compute the velocity vector 2D needed to go from init_pos to target_pos in time_of_flight seconds.
    Only gravity force is taken into account.
    Formula used is : height(t) = vertical_initial_speed * time - (gravity * time * time * 0.5)
    We want to resolve the equation for t=T, when T is equal to the time needed to reach the destination. 
    We know it since it's a variable provided. So, for t=T, height(T) is in reality final_height, also called target_pos.y

    Gravity is 1960, or the double of 980, which is the default grav in Godot (instead of 9.81)

    Source : https://www.nagwa.com/fr/explainers/160105648490/
    """
    initial_velocity = numpy.array([0,0])

    initial_velocity[0] = (target_pos[0] - init_pos[0])/time_of_flight

    initial_velocity[1] = (target_pos[1] - init_pos[1] + gravity*time_of_flight*time_of_flight*0.5)/time_of_flight

    return initial_velocity

init_pos = numpy.array([0,0])
target_pos = numpy.array([630,10])
time_of_flight = 2

print(compute(init_pos,target_pos,time_of_flight))


