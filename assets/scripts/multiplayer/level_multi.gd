extends Node

class_name LevelMulti

signal game_over
signal pause


@export var offset_camera := 700
var collected_coin := 0

######## ADDED FOR MULTI
@export var PlayerScene: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	var index = 0
	
	######## ADDED FOR MULTI
	for i in PeerManager.Players:
		var currentPlayer = PlayerScene.instantiate()
		currentPlayer.name = str(PeerManager.Players[i].id)
		add_child(currentPlayer)
		for spawn in get_tree().get_nodes_in_group("PlayerSpawnPoint"):
			if spawn.name == str(index):
				currentPlayer.global_position = spawn.global_position
		index += 1
		
	
	
	# Randomize the seed for the PRNG (based on system clock)
	randomize()
	process_mode = Node.PROCESS_MODE_PAUSABLE
	$HUD/CoinScore.text = "Coins\n0"
	
	

	


##### Trap handling management

func _on_trigger_new_trap(trap):
	trap.hit.connect(_on_trap_hit_player)

func _on_trap_hit_player():
	$Hit.play()
	$HUD/HitLog.text += "\n HIT"
	game_over.emit()

##### Coin handling management

func _on_trigger_new_coin(coin):
	coin.body_entered.connect(_on_coin_body_entered)

func _on_coin_body_entered(body):
	if body is Player:
		collected_coin += 1
		$HUD/CoinScore.text = "Coins\n" + str(collected_coin)

func _on_button_pressed():
	pause.emit()


