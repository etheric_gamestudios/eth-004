extends CharacterBody2D
class_name  PlayerMulti


var first_jump := Vector2(-500*100,-8*100)
var second_jump := Vector2(-500*100,-8*100)
var third_jump := Vector2(-500*100,-9*100)
var current_jump: Vector2 = first_jump
var number_of_jump_available = 3

# Get the gravity from the project settings so you can sync with rigid body nodes.
var gravity := 1800

var jumped_right := true

var just_jumped := false

var is_wall_glued := false

func _ready():
	$MultiplayerSynchronizer.set_multiplayer_authority(str(name).to_int())
	print("M AUTH ",$MultiplayerSynchronizer.get_multiplayer_authority())
	print("My ID is : ", multiplayer.get_unique_id())
	if $MultiplayerSynchronizer.get_multiplayer_authority() == multiplayer.get_unique_id():
		add_child(Camera2D.new())

func _physics_process(delta):
	if $MultiplayerSynchronizer.get_multiplayer_authority() == multiplayer.get_unique_id():
		# Add the gravity.
		velocity.y += gravity * delta

		if is_on_floor():
			velocity.x = 0 # By default godot make the object slide
			number_of_jump_available = 3
			
		if is_wall_glued:
			velocity.y = 0

		
		var collider: KinematicCollision2D = get_last_slide_collision()
		if collider != null :
			var col = collider.get_collider()
			if col is StaticBody2D:
				if just_jumped == true:
					just_jumped = false
					jumped_right = not jumped_right
					$WallGlue.start()
					is_wall_glued = true
					
				number_of_jump_available = 3

					
			
		# Handle Jump.
		if Input.is_action_just_pressed("jump"):
			match (number_of_jump_available):
				3:
					current_jump = first_jump;
				2:
					current_jump = second_jump;
				1:
					current_jump = third_jump;

			if number_of_jump_available > 0:
				is_wall_glued = false
				velocity.y = current_jump.y
				if jumped_right:
					velocity.x = - current_jump.x * delta
				else:
					velocity.x = current_jump.x * delta
				
				just_jumped = true
				number_of_jump_available -= 1
			
			
		move_and_slide()

		


func _on_wall_glue_timeout():
	is_wall_glued = false
