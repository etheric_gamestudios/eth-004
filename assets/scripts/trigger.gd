extends Node2D


signal new_trap_signal(trap: Trap)
signal new_coin_signal(coin: Area2D)


# Trap
@export var spike_trap_scene: PackedScene
@export var electric_trap_palpi_scene: PackedScene
@export var electric_trap_pole_scene: PackedScene
@export var wall_spike_scene: PackedScene
@export var platform_scene: PackedScene
@export var laser_trap_scene: PackedScene


# Environment
@export var wall_container_scene: PackedScene
@export var coin_scene: PackedScene



var current_wall_pos: Vector2

var current_trap_area_pos := Vector2(202.5, -400)

var main_node: Node

var list_generation: Array[Trap] = []

# Amount of trap in the 4x4 Grid. 
var difficulty := 2

# Called when the node enters the scene tree for the first time.
func _ready():
	var wall_current = get_node("../WallContainer1")
	current_wall_pos = wall_current.position
	current_wall_pos.y -= 1000
	main_node = get_node(^"../")
	
	
	# Prototype for smart spawning

	# REGEX .*var (.*):.*     REPLACE  list_generation.append(\1.instantiate())
	list_generation.append(spike_trap_scene.instantiate())
#	list_generation.append(electric_trap_palpi_scene.instantiate())
#	list_generation.append(electric_trap_pole_scene.instantiate())
#	list_generation.append(wall_spike_scene.instantiate())
#	list_generation.append(platform_scene.instantiate())
	list_generation.append(laser_trap_scene.instantiate())
	
	# Support for tall screen, generate three first row of walls
	generate_wall()

	



# Instanciate a new Wall Container, 1000px higher than the current one
func _on_wall_trigger_body_entered(body):
	if body is CharacterBody2D:
#		print("It's a player")
		generate_wall() # One wall

# Create 4 virtual segment :
# - the upper is empty
# - the mid up is a trap segment
# - the mid low is empty
# - the lower is a trap segment 
# A trap segment separated in 4 area, 225 px wide (900px /4)
# We spawn in each area a trap
# First Segment visible on y=280
# Very first Center X : 202.5, other +225
# Very first Center Y :- 400
func _on_trap_trigger_body_entered(body):
	if body is CharacterBody2D:
#		var x_coordinates := [877.5,652.5,427.5,202.5]
		var x_coordinates := [202.5,427.5,652.5,877.5]
		var y_coordinates_offset := [600, 400, 200, 0]
#		print("It's a player")
		var grid = 	[[0, 0, 0, 0],
					 [0, 0, 0, 0],
					 [0, 0, 0, 0],
					 [0, 0, 0, 0]]
					
		# Loop for 'difficulty' amount of trap
		for i in range(0,difficulty):			
			# Spawn the trap at random location
			var x_random: int = randi_range(0,len(grid[0]) - 1 )
			var y_random: int = randi_range(0,len(grid) - 1)
			# Chose a different location if this one is already taken
			while grid[x_random][y_random]==1:
				x_random = randi_range(0,len(grid[0]) - 1 )
				y_random = randi_range(0,len(grid) - 1 )
				
			
			# Randomly chose a trap
			var chosen_trap = pick_a_trap().duplicate() as Trap
			# Check if the chosen trap can fit in the grid
			while can_place_trap(grid, chosen_trap, x_random, y_random) == false:
				chosen_trap = pick_a_trap().duplicate() as Trap
				
			# Fit the trap inside the grid, filling 1 instead of 0 inside the grid var
			placer_forme(grid, chosen_trap, x_random, y_random)
			
			# DEBUG
			print("Grid : ")
			print_grid(grid)
			
			
			# Actually instantiate the trap into the real world
			chosen_trap.position.x = x_coordinates[y_random]
			chosen_trap.position.y = current_trap_area_pos.y - y_coordinates_offset[x_random]
			new_trap_signal.emit(chosen_trap)
			add_child_deferred(chosen_trap)
		
		# DEBUG
		get_node(^"../Debug").position.y = current_trap_area_pos.y + 1100
		
		# Once all traps have been placed inside the zone, offset the grid higher on the screen			
		current_trap_area_pos.y -= 1000
		$TrapTrigger.position.y -= 1000
		

			

func print_grid(grid: Array):
	for row in grid:
		print(row)
		
# Verifiy if a shape can fit in a given grid, at the position x,y
func can_place_trap(grille, forme, x, y):
	print("Verify if %s fit at (%s,%s)" % [forme.size, x, y])
	for i in range(forme.size.x):
		for j in range(forme.size.y):
			# Vérifier les limites de la grille
			if x + i >= 4 or y + j >= 4:
				return false
			# Vérifier si la case est déjà occupée
			if grille[x + i][y + j] == 1:
				return false
	return true
		
		
# Fonction pour placer la forme dans la grille
func placer_forme(grille, forme, x, y):
	for i in range(forme.size.x):
		for j in range(forme.size.y):
			grille[x + i][y + j] = 1
			
			
func generate_traps():
	var chosen_trap = pick_a_trap().duplicate() as Trap
	
	
	chosen_trap.initialize()
	chosen_trap.position.y = current_wall_pos.y
	new_trap_signal.emit(chosen_trap)
	add_child_deferred(chosen_trap)

		
func pick_a_trap() -> Trap:
	
	# Let's assume two trap in the list
	# Spike trap 100
	# Elec trap 50
	# Total proba = 100 + 50 = 150
	# Proba for spike trap : 100/150 = 0.66
	# Proba for elec trap : 50/150 = 0.33
	
	
	# Proto for smart trap spawning mechanism
	var list_trap_eligible: Array[Trap] = []
	var total_weight: int = 0
	for trap in list_generation:
#		print("GEN Trap Name : %s, ponderation %s, alt_needed %s, cum_w %s" % [trap.name, trap.spawn_ponderation, trap.alt_needed, trap.cumulative_weight])
		if trap.alt_needed > global_position.y:
			list_trap_eligible.append(trap)
			total_weight += trap.spawn_ponderation
			trap.cumulative_weight = total_weight
			
			
	var roll: int = randi_range(0,total_weight)
#	print("roll %s"%roll)
	var chosen_trap: Trap
	for trap in list_generation:
		if trap.cumulative_weight >= roll:
			chosen_trap = trap
			break
	
	
#	print("Trap chosen : ", chosen_trap)

	return chosen_trap
		


	
		

		
func generate_platform():
	var platform = platform_scene.instantiate()
	
	# Segment from 200 to 880, we let 230 px wide to have the space to wall jump
	# 200 + 230 px = 430
	# 880 - 230 = 650
	var pos_x = randi_range(430,650)
	platform.position = Vector2(pos_x, current_wall_pos.y)
	add_child_deferred(platform)
	
	
func generate_coin():
	var coin = coin_scene.instantiate()
	
	# Segment from 200 to 880
	var pos_x = randi_range(210,860)
	coin.position = Vector2(pos_x, current_wall_pos.y)
#	print("Coin position",coin.position)
	
	new_coin_signal.emit(coin)
	add_child_deferred(coin)
	
func generate_wall():
	var wall = wall_container_scene.instantiate()
	wall.position = Vector2(current_wall_pos.x, current_wall_pos.y - 1000)
	add_child_deferred(wall)
	
	current_wall_pos.y -= 1000
	$WallTrigger.position.y -= 1000
		

## Godot debugger complains that the call has not been deferred, so there it is !
func add_child_deferred(object):
	main_node.call_deferred(&"add_child",object)



func _on_visible_on_screen_notifier_2d_screen_exited():
	print("t")




