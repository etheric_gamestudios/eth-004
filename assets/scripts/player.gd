extends CharacterBody2D
class_name  Player


var first_jump := Vector2(-630,-880)
var second_jump := Vector2(-500,-15*100)
var third_jump := Vector2(-500,-11*100)

#var ground_jump := Vector2(-680,-590) # t=1
#var ground_jump := Vector2(-315,-1965) #t=1, end pos 680,10
var ground_jump := Vector2(-630,-990) #t=1, g=2g  GOOD


#var ground_jump := Vector2(-590,-590) #t=2

var current_jump: Vector2 = first_jump
var number_of_jump_available = 3


@export var is_invicible := false

# Get the gravity from the project settings so you can sync with rigid body nodes.
var gravity = 2*ProjectSettings.get_setting("physics/2d/default_gravity")

var jumped_right := true

var just_jumped := false

var is_wall_glued := false



func _physics_process(delta):
	# Add the gravity.
	velocity.y += gravity * delta
	

	if is_on_floor():
		velocity.x = 0 # By default godot make the object slide
		number_of_jump_available = 3
		
	if is_wall_glued:
		velocity.y = 0

	
	var collider: KinematicCollision2D = get_last_slide_collision()
	if collider != null :
		var col = collider.get_collider()
		if col is StaticBody2D:
			if just_jumped == true:
				just_jumped = false
				jumped_right = not jumped_right
				$WallGlue.start()
				is_wall_glued = true
				
			number_of_jump_available = 3

				
		
	# Handle Jump.
	if Input.is_action_just_pressed("jump"):
		match (number_of_jump_available):
			3:
				current_jump = first_jump;
			2:
				current_jump = second_jump;
			1:
				current_jump = third_jump;

		if number_of_jump_available > 0:
			is_wall_glued = false
			velocity.y = current_jump.y
			if jumped_right:
				velocity.x = - current_jump.x
			else:
				velocity.x = current_jump.x
			
			just_jumped = true
			number_of_jump_available -= 1
			
		if is_on_floor():
			velocity.y = ground_jump.y
			if jumped_right:
				velocity.x = - ground_jump.x
			else:
				velocity.x = ground_jump.x
			
	
	## Animation HANDLING
	if jumped_right:
		$AnimatedSprite2D.flip_h = false
	else:
		$AnimatedSprite2D.flip_h = true
	if velocity.y > 1.2*(gravity * delta):
		$AnimatedSprite2D.play("falling")
	elif velocity.y < -1.2*(gravity * delta):
		$AnimatedSprite2D.play("ascending")
	else:
		$AnimatedSprite2D.play("idle")
	
	move_and_slide()

func _on_wall_glue_timeout():
	is_wall_glued = false

func become_invicible(time: float):
	$InvicibilityTimer.wait_time = time
	$InvicibilityTimer.start()
	is_invicible = true
	
	# Show shield animation and play it
	$Shield.show()
	$Shield.play()


func _on_invicibility_timer_timeout():
	is_invicible = false
	$Shield.hide()
	$Shield.stop()
	


