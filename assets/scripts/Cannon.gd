extends Area2D

@export var cannon_force := 35

var start_ang = PI/4
var end_ang = 3*PI/4
var current_target = end_ang
var angular_velocity = PI/4

var body_inside: Player

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if global_rotation < current_target:
		rotate(angular_velocity * delta)
		current_target = end_ang
	elif global_rotation >= current_target:
		current_target = start_ang
		rotate(-angular_velocity * delta)
	


func _on_body_entered(body):
	if body is Player:
		
		print("Entered cannon")
		body.process_mode = CollisionObject2D.PROCESS_MODE_DISABLED
		body.is_invicible = true # set before hiding so VisibleOnScreenNotifier doesn't fire
		body.hide()
		body.position = position
		$AnimatedSprite2D.play("firing")
		$FiringTimer.start()
		body_inside = body



func _on_firing_timer_timeout():
	body_inside.process_mode = CollisionObject2D.PROCESS_MODE_INHERIT
	body_inside.show() 
	$AnimatedSprite2D.play("idle")
	body_inside.velocity = cannon_force*($Target.global_position - global_position)
	$CollisionShape2D.disabled = true
	body_inside.become_invicible(2)
