extends Node

class_name Level

signal game_over
signal pause

@export var offset_camera := 700
@export var camera_following := true
var collected_coin := 0

var total_coin :int


var max_height :float

# Called when the node enters the scene tree for the first time.
func _ready():
	
	# Randomize the seed for the PRNG (based on system clock)
	randomize()
	process_mode = Node.PROCESS_MODE_PAUSABLE
	$HUD/CoinScore.text = "Coins\n0"
	max_height = $Player.position.y - 1920
	
	


func display_height(height:float):
	
	$HUD/ScoreCounter.text = "%s m" % str(height)


func _process(_delta):
	
	# Camera management, adjust dynamically the bottom limit
	if camera_following:
		if $Player/Camera2D.limit_bottom > $Player.position.y + offset_camera:
			$Player/Camera2D.limit_bottom = $Player.position.y + offset_camera
		
	# Negative Y coordinate	
	var current_height = $Player.position.y - 1920
	if current_height < max_height:
		max_height = current_height
	
	var _human_readable_height = round(abs(max_height)/100)
	display_height(_human_readable_height) 

##### Trap handling management

func _on_trigger_new_trap(trap):
	trap.hit.connect(_on_trap_hit_player)

func _on_trap_hit_player(player: Player):
	if not player.is_invicible:
		$HitSound.play()
		$HUD/HitLog.text += "\n HIT"
		game_over.emit()

##### Coin handling management

func _on_trigger_new_coin(coin):
	coin.body_entered.connect(_on_coin_body_entered)

func _on_coin_body_entered(body):
	if body is Player:
		collected_coin += 1
		$HUD/CoinScore.text = "Coins\n" + str(collected_coin)

func _on_button_pressed():
	pause.emit()




func _on_visible_on_screen_notifier_2d_screen_exited():
	_on_trap_hit_player($Player)


func _on_save_game_button_pressed():
	save_game()

func save_game():
	var save_game_file :FileAccess = FileAccess.open("user://savegame.save", FileAccess.WRITE)
	var save_data = {"max_score":max_height, "coins":total_coin + collected_coin}
	var json_string = JSON.stringify(save_data)
	save_game_file.store_line(json_string)
	print("Game state saved at %s " % save_game_file.get_path_absolute())



func _on_load_score_game_button_pressed():
	load_game()
	
func load_game():
	if not FileAccess.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.
		
	var save_game_file = FileAccess.open("user://savegame.save", FileAccess.READ)
	var json_string = save_game_file.get_line()
	var json = JSON.new()
	
	# Check if there is any error while parsing the JSON string, skip in case of failure
	var parse_result = json.parse(json_string)
	if not parse_result == OK:
		print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
		return
		
	var node_data = json.get_data()
	var _max_score = round(abs(node_data["max_score"])/100)
	$HUD/MaxScore.text = "Max Score : %s m" % _max_score
