extends Node

@export var level_scene: PackedScene

var current_level: Level

func _ready():
	process_mode = Node.PROCESS_MODE_ALWAYS
	
	
func _process(_delta):
	if Input.is_action_just_pressed("pause") and get_tree().paused == false:
		pause()
		

func play_level():
	var level: Level = level_scene.instantiate()
	level.game_over.connect(on_level_game_over)
	level.pause.connect(pause)
	current_level = level
	add_child(level)


func on_level_game_over():
#	print("game over signal recevied")
	pass


func pause():
	get_tree().paused = true
	$PauseMenuContainer.show()

func _on_play_button_pressed():
	play_level()
	$MainMenuContainer.hide()


func _on_option_pressed():
	$MainMenuContainer.hide()
	$OptionContainer.show()


func _on_option_back_pressed():
	$OptionContainer.hide()
	if get_tree().paused:
		$PauseMenuContainer.show()
	else:
		$MainMenuContainer.show()


func _on_resume_button_pressed():
	$PauseMenuContainer.hide()
	get_tree().paused = false


func _on_resume_option_pressed():
	$PauseMenuContainer.hide()
	$OptionContainer.show()


func _on_back_pause_menu_pressed():
	$PauseMenuContainer.hide()
	$MainMenuContainer.show()
	get_tree().paused = false
#	current_level.queue_free()
	$Level.queue_free()
