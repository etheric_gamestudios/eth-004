extends Trap

class_name SpikeTrap


@export var move_speed: float = 50.0
@export var hor_move_dir: Vector2
@export var vert_move_dir: Vector2



var start_pos
var target_pos 
var move_dir 

enum Behavior {STATIONARY, HORIZONTAL, VERTICAL}

var mode

func _ready():
	super()
	start_pos = global_position
	
	match mode:
		Behavior.HORIZONTAL:
			move_dir = hor_move_dir
			target_pos = start_pos + move_dir
		Behavior.VERTICAL:
			move_dir = vert_move_dir
			target_pos = start_pos + move_dir
		Behavior.STATIONARY:
			pass
	
	
func init(_mode := Behavior.STATIONARY):
	self.mode = _mode


func _process(delta):
	
	if mode == Behavior.HORIZONTAL or mode==Behavior.VERTICAL:
		global_position = global_position.move_toward(target_pos, move_speed * delta)
		if global_position == target_pos:
			if global_position == start_pos:
				target_pos = start_pos + move_dir
			else:
				target_pos = start_pos
	
	
	#  Apply a rotation of 2 PI rad/s (360°/s), bc for example for 60 fps, it needs 60 delta to have one second and reach 2PI
	rotate(2*PI * delta)

func initialize():
		var _mode = randi_range(0,len(SpikeTrap.Behavior)-1)
		init(_mode) #Init with random trap behavior
		print("Mode chosen ",_mode)
		
		# playable X segment :  from 200 to 880 = 680 px wide
		# We need to account for delta dir of x=50 (horiz movmt) of the spike trap
		# So, segment available is 680 - 50 = 630 px wide, starting from 200
		var pos_x = randi_range(200,630) # generate a number between 1 and 630
		position = Vector2(pos_x, 0)
		

func _on_body_entered(body):
	## Overriden trap detection to generate particles
	super(body)
	$GPUParticles2D.emitting = true
