extends Trap


var enabled := false


## Start ELECTRIZING
func _on_anim_timer_timeout():
	$ArcTimer.start()
	$ElecSprite.play("arc")
	$CollisionShape2D.disabled = false


## Stop ELECTRIZING
func _on_arc_timer_timeout():
	$IdleTimer.start()
	$ElecSprite.play("idle")
	$CollisionShape2D.disabled = true


func initialize():
		position = Vector2(555, 0)
