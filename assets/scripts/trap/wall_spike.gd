extends Trap

class_name WallSpikeTrap

enum Behavior {TO_RIGHT, TO_LEFT}


func init(alignement: Behavior):
	if alignement == Behavior.TO_RIGHT:
		pass
	elif alignement == Behavior.TO_LEFT:
		rotation = -PI/2
		

func initialize():
	
	var _mode = randi_range(0,len(WallSpikeTrap.Behavior)-1)
	init(_mode) #Init with random trap behavior
	var pos_x
	if _mode == WallSpikeTrap.Behavior.TO_LEFT:
		pos_x = 880
	else:
		pos_x = 200
		
	position = Vector2(pos_x, 0)
		

