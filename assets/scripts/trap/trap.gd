extends CollisionObject2D

## Base custom class which inherit all other trap
class_name Trap

signal hit(player)

## Minimum altitude needed to spawn this trap, can be used to spawn high_difficulty trap only after a certain distance. [br]
## By default 1920, meaning trap can spawn as soon as the player enter the game
var alt_needed: int = 1920

## Spawn index which translate to spawn probability. Only applicable when alt_needed is reach. [br]
## By default 100, the higher the index the higher the spawn proba. 
var spawn_ponderation: int = 100

## Used for random picking choice. 
## See http://kehomsforge.com/tutorials/single/GDWeightedRandom
var cumulative_weight: int

var has_been_visible := false

# A size of 1,1 means that the trap take 200x200 px wide
# The game trap generation area is divided into 4 zone where a 1x1 trap can fit
# See trigger script for more detail
# A size of 2x1 means 400x200 px wide
var size: Vector2 = Vector2(1,1)

func _ready():
	var screen_notifier := VisibleOnScreenNotifier2D.new()
	screen_notifier.screen_exited.connect(screen_visibility_handler.bind(false))
	screen_notifier.screen_entered.connect(screen_visibility_handler.bind(true))
	screen_notifier.rect.position = Vector2(-10,-200) # Allow the trap to disappear long after the camera has passed
	add_child(screen_notifier)
	
	
func screen_visibility_handler(status: bool):
	# Check if the screen entered the screen a first time at least before deleting it..
	# Param : status, if true, object entered, if false, object exited
	
	if status == true:
		has_been_visible = true
		
	if status == false && has_been_visible == false:
		# Do nothing, object is spawned outside the screen
		pass
	elif status == false && has_been_visible == true:
		#queue_free()
		pass


	
func _on_body_entered(body):
	if body is Player:
		hit.emit(body)
		print("HIT")

## Method have to be overrided by the inherited trap [br]
## Initialize the trap Behavior (static, moving...) and x position
func initialize():
	pass
