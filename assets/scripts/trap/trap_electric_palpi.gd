extends Trap

var frame_number: int
var enabled := false

func _ready():
	super()
	alt_needed = -2000
	
	frame_number = $ElecSprite.sprite_frames.get_frame_count("arc")
	$Nixie.play()
	init(Mode.TO_LEFT)
	
enum Mode {TO_LEFT,TO_RIGHT}

func init(mode):
	if mode == Mode.TO_LEFT:
		$Palpatine.flip_h = !$Palpatine.flip_h
		$ElecSprite.position.x = -$ElecSprite.position.x
		$CollisionShape2D.position.x = -$CollisionShape2D.position.x

func _on_anim_timer_timeout():
	if enabled:
		$ElecSprite.show()
		$ElecSprite.frame = randi() % frame_number
		$Palpatine.play()
		
	else:
		$ElecSprite.hide()
		$Palpatine.stop()

		


func _on_schedule_timer_timeout():
	enabled = !enabled
	if enabled:
		$SoundEffect.play()
		$CollisionShape2D.disabled = false
	else:
		$SoundEffect.stop()
		$CollisionShape2D.disabled = true


func initialize():
		var _mode = randi_range(0,len(Mode)-1)
		init(_mode) #Init with random trap behavior
		
		var pos_x = randi_range(200,630) # generate a number between 1 and 630
		position = Vector2(pos_x, 0)
