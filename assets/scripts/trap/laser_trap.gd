extends Trap




var move_speed = 300

func _init():
	# This is a 2x2 trap (400x400px)
	size = Vector2(2,2)
	pass
	
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if $CollisionLaser.position < $EndPoint.position:
		$CollisionLaser.position = $CollisionLaser.position.move_toward($EndPoint.position, move_speed * delta)
	else:
		$CollisionLaser.position = $StartingPoint.position
		
		
func _on_body_entered(body):
	## Overriden trap detection to generate particles
	super(body)
	$GPUParticles2D.emitting = true
